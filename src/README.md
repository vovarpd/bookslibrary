# Books Library

- public folder is located under /public_html (DOCUMENT_ROOT)
- app src is located in /src (outside of DOCUMENT_ROOT)

## Installation
- cd /src
- Fill MySQL credentials in .env.example and rename .env.example to .env 
- Run migrations: php artisan migrate
- Go to url: /upload
- Upload XML file
- Run php artisan queue:work to run queued jobs (you may configure [supervisor](https://laravel.com/docs/8.x/queues#supervisor-configuration))
- Wait to finish.
- Optional: run url "/upload/process" to re-import uploaded XML into DB.
