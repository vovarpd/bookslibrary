@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">{{config('app.name')}}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ISBN</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                @foreach ($books as $book)
                                    <tr>
                                        <td class="nowrap">{{$book->isbn}}</td>
                                        <td>{{$book->title}}</td>
                                        <td class="w-25"><img alt="" src="{{$book->image}}" class="img-thumbnail"></td>
                                        <td>{{\Illuminate\Support\Str::substr($book->description,0,50)}}</td>
                                    </tr>
                                @endforeach
                            </table>

                            {{ $books->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
