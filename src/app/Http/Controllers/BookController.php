<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookUploadRequest;
use App\Jobs\BookXmlProcess;
use App\Models\Book;
use App\Services\HandleXmlUpload;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books=Book::paginate(100);
        return view('welcome')
            ->with('books',$books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }

    public function upload(){
        return view('books.upload');
    }

    public function uploadStore(BookUploadRequest $request){
        $data=$request->validated();
        $handleXmlUpload=new HandleXmlUpload();
        $handleXmlUpload->setFile($data['file']);
        $handleXmlUpload->run();
        return back()->with('status','File has been uploaded and queued.');
    }

    public function uploadProcess(){
        if(!Storage::disk(HandleXmlUpload::XML_DISK)->exists(HandleXmlUpload::XML_FILENAME)){
            return back()->withErrors(['xml_file_not_found'=>'Please upload XML file firstly.']);
        }
        BookXmlProcess::dispatchNow(HandleXmlUpload::XML_FILENAME,HandleXmlUpload::XML_DISK);
    }
}
