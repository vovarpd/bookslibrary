<?php


namespace App\Services;


use App\Jobs\BookXmlProcess;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class HandleXmlUpload
{
    protected $xml;
    const XML_FILENAME='books.xml';
    const XML_DISK='local';

    public function __construct(?UploadedFile $xml = null)
    {
        $this->xml = $xml;
    }

    public function setFile(UploadedFile $xml)
    {
        $this->xml = $xml;
    }

    public function run()
    {
        if(Storage::disk(self::XML_DISK)->exists(self::XML_FILENAME)){
            Storage::disk(self::XML_DISK)->delete(self::XML_FILENAME);
        }
        $this->xml->storeAs('/',self::XML_FILENAME,self::XML_DISK);
        BookXmlProcess::dispatch(self::XML_FILENAME,self::XML_DISK);
    }
}
