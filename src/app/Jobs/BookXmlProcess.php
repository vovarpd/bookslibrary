<?php

namespace App\Jobs;

use App\Models\Book;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Prewk\XmlStringStreamer;

class BookXmlProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600;
    public $tries = 3;

    protected $disk;
    protected $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $filename, string $disk)
    {
        $this->filename = $filename;
        $this->disk = $disk;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Factory $storage)
    {
        if (PHP_SAPI !== 'cli') {
            ini_set('output_buffering', 'off');
        }
        $filename_path = $storage->disk($this->disk)->path($this->filename);
        $streamer = XmlStringStreamer::createStringWalkerParser($filename_path, [
            'captureDepth' => 3
        ]);
        $current_item = 0;
        $chunk = [];
        while ($node = $streamer->getNode()) {
            try {
                $current_item++;
                $item = simplexml_load_string($node);

                /** @var $item \SimpleXMLElement */
                $attributes = $item->attributes();
                if (empty((string)$attributes['isbn']) || empty((string)$attributes['title'])) {
                    throw new \Exception('Invalid node. ISBN & Title must be filled.');
                }
                $data = [];
                $data['isbn'] = (string)$attributes['isbn'];
                $data['title'] = (string)$attributes['title'];
                $data['image'] = (string)$item->image;
                $data['description'] = (string)$item->description;
                $chunk[] = $data;
                if (count($chunk) === 1000) {
                    // insert
                    Book::query()->upsert($chunk, 'isbn');
                    $chunk = [];
                    if (PHP_SAPI !== 'cli') {
                        echo '#' . $current_item . ' has been processed.<br>';
                    }
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        }

        if (count($chunk) > 0) {
            // insert remaining chunk items
            Book::query()->upsert($chunk, 'isbn');
        }

    }
}
