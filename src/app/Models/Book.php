<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App\Models
 * @property string $isbn
 * @property string $title
 * @property string $image
 * @property string $description
 *
 * @mixin Builder
 */
class Book extends Model
{
    use HasFactory;
}
